var express = require('express');
var bodyParser = require('body-parser');
var dataFile = require('./server/data/data.json');
var logger = require('morgan');
var errorHandler = require('errorhandler');
var http = require('http');
var sass = require('node-sass-middleware'); // We're adding the node-sass module
var path = require('path');      // Also loading the path module
var helmet = require('helmet');
var Middleware = require('./server/misc/Middleware');
var compression = require('compression');
var async = require('async');
var app = express();


app.set('port', process.env.PORT || 3000 );
app.set('appData', dataFile);
app.set('view engine', 'pug');

// parse application/json 
app.use(bodyParser.urlencoded({ extended: true })) // support encoded bodies
app.use(bodyParser.json());

app.locals.siteTitle = 'Partner Programme';

/*****************************************/

app.use(express.static( path.join( __dirname, 'client/public' ) ) );
app.set('views',  path.join( __dirname, 'client/views/' ));
//app.use('css',express.static(path.join(__dirname, './client/public/')));

var srcPathcss = path.join(__dirname,'precomp/sass/');
var destPathcss =  path.join(__dirname,'client/public/css/');

var srcPathjs = path.join(__dirname,'precomp/js/');
var destPathjs =  path.join(__dirname,'client/public/js/');

var srcPathAngular = path.join(__dirname,'precomp/angular/');
var destPathangular =  path.join(__dirname,'client/public/angular2/');

function parallel(middlewares) {
  return function (req, res, next) {
    async.each(middlewares, function (mw, cb) {
      mw(req, res, cb);
    }, next);
  };
}

app.use(parallel([
    sass({
           src: srcPathcss
         , dest: destPathcss
         , prefix:  '/css'
         , outputStyle: 'compressed'
         , response: false
         , debug: false
    })
    ,logger('combined')
    ,helmet({frameguard: false })
    //Middleware.expressValidator,
    ,compression()
]));


// The static middleware must come after the sass middleware
app.use(errorHandler({ dumpExceptions: true, showStack: true }));

//app.use(helmet());

app.locals.allDestinations = dataFile.destinations;
app.locals.footer = dataFile.Footer;

//app.use(Middleware.expressValidator);

require('./server/routes/routes')(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: {}
  });
});

module.exports = app;