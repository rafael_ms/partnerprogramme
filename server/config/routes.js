var index = require('../controller/index');
var trips = require('../controller/trips');
var register = require('../controller/register');
var data = require('../data/data.json');

module.exports = function(app){
		app.get('/', index.home);
		app.get('/noosa', trips.noosa);
		app.get('/buenos-aires', trips.buenos);
		app.get('/vip', trips.vip);
		app.get('/register', register.home);
		app.get('*', function(req, res, next){
			var err = new Error();
			err.status = 404;
			index.notfound;
		});
}