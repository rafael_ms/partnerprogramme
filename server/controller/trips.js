exports.noosa = function(request, response){
	var data = request.app.get('appData');

    response.render('index', {
		    pageTitle: 'Noosa',
		    cover: data.destinations[0].cover,
		    menu: data.home[0].menu,
		    heading:data.destinations[0].heading,
		    details: data.destinations[0].details,
		    pageID: 'noosa'
		});
};

exports.buenos = function(request, response){
	var data = request.app.get('appData');
	//delete req.session;
	response.render('index', {
	    pageTitle: 'Buenos Aires',
	    cover: data.destinations[1].cover,
		menu: data.home[0].menu,
		heading:data.destinations[1].heading,
		details: data.destinations[1].details,
	    pageID: 'buenos-aires'
	});
};

exports.vip = function(request, response){
	var data = request.app.get('appData');

	response.render('index', {
	    pageTitle: 'Vip',
	    cover: data.destinations[2].cover,
		menu: data.home[0].menu,
		heading:data.destinations[2].heading,
		details: data.destinations[2].details,
	    pageID: 'vip'
	});
};
