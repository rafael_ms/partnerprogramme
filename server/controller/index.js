exports.home = function(request, response){
	var data = request.app.get('appData');

    response.render('index');
};

exports.partials = function(request, response){
    var name = request.params.name;
    response.render('partials/content/' + name);
}

exports.content = function(request, response){
	var data = request.app.get('appData');

	var req = request.params.req;

	switch(req){
		case 'get-Home':
			data.home.pageID = 'home';
			response.json(data.home);
			break;
		case 'get-trip':
			var trip = request.params.tripname;

			var result = data.destinations[trip];

			if(typeof result !== 'undefined'){
				result.pageID = 'trips';
				result.menu = data.home.menu;

				response.json(result);
			}
			else
				return response.json(result);
			break;
		default:
			data.home.pageID = 'not-found';
			response.json(data.home);
	}
}

exports.notfound = function(request, response){
	var data = request.app.get('appData');

    response.render('index', {
			    pageTitle: 'Page Not Found',
			    cover: data.home[0].cover,
			    menu: data.home[0].menu,
			    heading:data.home[0].heading,
			    pageID: 'NotFound'
			});
};