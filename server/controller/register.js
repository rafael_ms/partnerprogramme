module.exports.home = function(request, response){
	var data = request.app.get('appData');

	response.render('index', {
	    pageTitle: 'Register',
	    cover: data.register[0].cover,
		menu: data.home[0].menu,
		heading: data.register[0].heading,
	    pageID: 'register'
	});
};
var formidable = require("formidable");
var util = require('util');

module.exports.form = function(request, response){
	var data = request.app.get('appData');
	var util = require('util')
	var helper = require('sendgrid').mail;
	var from_email = new helper.Email('noreply@partnetprogramme.com');
	var to_email = new helper.Email('tania@wth.co.nz'); //tania@wth.co.nz CHANGE TO
	var subject = 'Partner Programme - Website Form Submission';
	var page = "";

	var passenger1 = request.fields.filter(function(item){
		return item.Name.indexOf('0') > 0;
	});
	var passenger2 = request.fields.filter(function(item){
		return item.Name.indexOf('2') > 2;
	});

	response.render('email', {
			    passenger1 : passenger1,
			    passenger2 : passenger2,
			    fieldName1 : data.PassengerDetails[0].friendlyNames1,
			    fieldName2 : data.PassengerDetails[0].friendlyNames2
			},function(err, html) {
		        page = html;
		        response.send(JSON.stringify([ true, 'Sucessfully submitted!', '#00000' ]));
		    });
	var content = new helper.Content('text/html', page + ' ');
	var mail = new helper.Mail(from_email, subject, to_email, content);


	var sg = require('sendgrid')('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');
	var request = sg.emptyRequest({
	  method: 'POST',
	  path: '/v3/mail/send',
	  body: mail.toJSON(),
	});

	sg.API(request, function(error, response) {
	  console.log(response.statusCode);
	  console.log(response.body);
	  console.log(response.headers);
	});

};

module.exports.add = function(request, response){
	response.render('partials/template/registerform', {
			    id : 2
			},function(err, html) {
		        page = html;
		        response.send(JSON.stringify(page));
		    });
};