var index = require('../controller/index');
/*var trips = require('../controller/trips');
var register = require('../controller/register');
var data = require('../data/data.json');*/

module.exports = function(app){
		//app.get('/', index.home);
		
		app.get('/partials/content/:name', index.partials);

		app.get('/content/api/:req', index.content);

		app.get('/content/api/:req/:tripname', index.content);
		
		app.get('*', index.home);
		//app.get('/noosa', trips.noosa);
		//app.get('/buenos-aires', trips.buenos);
		//app.get('/vip', trips.vip);
		//app.get('/register', register.home);
		//app.post('/register', register.form);
		//app.post('/add', register.add);
}