var Validation = require('./validations');
var formidable = require("formidable");
var Validator = require('validator');
var util = require('util');

exports.expressValidator = function(request, response, next){
	if (request.url == '/register' && request.method === 'POST') {
		var form = new formidable.IncomingForm(),
			fields = [];
	    //form.parse(request, function (err, fields, files) {
	    //Store the data from the fields in your data store.
	    //The data store could be a file or database or any other store based
	    //on your application.

	    form
	      .on('error', function(err) {
	        response.writeHead(200, {'content-type': 'text/html'});
	        //response.end('error:\n\n' + util.inspect(err));
	        next();
	      })
	      .on('field', function(field, value) {
	        console.log(field, value);
	        fields.push({Name : field, Value : value});
	      })
	      .on('end', function() {
	        console.log('-> post done');
	    	var valid = [];

	    	try{
	    		valid = validate(fields);
	    	}
	    	catch(e){
				response.end(util.inspect( e ));
			}

	    	if(valid[0]){
	    		request.fields = fields;
	    		next();
	    	}
	    	else{
	    		response.writeHead(response.statusCode.toString());
	        	response.end( JSON.stringify([ valid[0], valid[1], valid[2] ]));
	    	}
	      });
	    form.parse(request);
	}
	else
		next();

}

function validate(fields){
	var errors = 0;
	var result = [true];
	var requiredFields = fields.filter(function(item){ return item.Name == 'required' })[0].Value.split(',');
	var emails = fields.filter(function(item){ return item.Name == 'email0' || item.Name == 'email1'});
	var Mphones = fields.filter(function(item){ return item.Name =='mobphone0' || item.Name =='mobphone1'});
	var Lphones = fields.filter(function(item){ return item.Name == 'workphone0' || item.Name == 'homephone0' || item.Name == 'emergencyphone0' || item.Name == 'workphone1' || item.Name == 'homephone1' || item.Name == 'emergencyphone1' });

	if(fields.length < 34)
		errors++;

	if(errors > 0)
		return [false, "Invalid Submission!", "#10001"];

	fields.forEach(function(item, key){
		if(requiredFields.indexOf(item.Name) != -1){
			if(Validation.IsNullOrEmpty(item.Value))
				errors++;
			item.Value = Validator.escape(item.Value);
		}
	});

	if(errors > 0)
		return [false, "Check the required fields!", "#10002"];

	emails.forEach(function(item, key){
		if(!Validator.isEmail(item.Value))
			errors++;
		item.Value = Validator.normalizeEmail(item.Value,[{all_lowercase: true}]);
	});

	if(errors > 0)
		return[false, "Check your email!", "#10003"];

	Mphones.forEach(function(item, key){
		if(!Validator.isMobilePhone(item.Value,'en-NZ'))
			errors++;
	});

	Lphones.forEach(function(item, key){
		if(!Validator.isNumeric(item.Value))
			errors++;
	});

	if(errors > 0)
		return [false, "Invalid phone number!", "#10004"];

	return result;
}

/// Errors
/// #10001 - Invalid amount of fields submitted
/// #10002 - Empty Fields
/// #10003 - Invalid email address
/// #10004 - Invalid Phone number
