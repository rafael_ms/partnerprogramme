'use strict';

angular.
  module('core.trip').
  factory('Trip', ['$resource',
    function($resource) {
      return $resource('trips/:tripId.json', {}, {
        query: {
          method: 'GET',
          params: {tripId: 'trips'},
          isArray: true
        }
      });
    }
  ]);
