'use strict';

// Define the `ppApp` module
angular.module('ppApp', ['ngRoute','ngSanitize','home','trips','notfound']);