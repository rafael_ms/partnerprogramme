'use strict';

angular.
  module('ppApp').
  config(['$routeProvider',
    function ($routeProvider) {
      $routeProvider.
        when('/',{
          template: '<home></home>'
          ,title: 'Partner Programme - Home'
        })
        .when('/trips/:tripId',{
          template: '<trips></trips>'
          ,title: 'Partner Programme - Trips'
        })
        .when('/notfound',{
          template: '<notfound></notfound>'
          ,title: 'Partner Programme - Not Found'
        })
        .otherwise({
          redirectTo: '/notfound'
        });
    }
  ])
  .config(['$locationProvider', function($locationProvider) {
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
  }])
  .run(['$rootScope', '$route', function($rootScope, $route) {
    $rootScope.$on('$routeChangeSuccess', function() {
        document.title = $route.current.title;
    });
  }]);;
