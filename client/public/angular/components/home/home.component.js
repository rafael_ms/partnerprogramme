'use strict';

// Register `tripDetail` component, along with its associated controller and template

angular.
  module('home')
  .controller('homeController', homeController)
  .component('home', {
    templateUrl: 'partials/content/content_main'
    ,controller: 'homeController'
  });

  function homeController($scope,$http){

      $http.get('/content/api/get-Home')
        .then(function(response){
          $scope.content = response.data
        })
        .catch(function(error){
          console.log(error);
        });
          //.success(function(response) { $scope.content = response })
          //.error(function() { console.log('Error when getting content'); });
  }
