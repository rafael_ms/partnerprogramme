'use strict';

// Register `tripDetail` component, along with its associated controller and template

angular.
  module('notfound')
  .controller('notfoundController', homeController)
  .component('notfound', {
    templateUrl: 'partials/content/content_404'
    ,controller: 'notfoundController'
  });

  function homeController($scope,$http){

 	  $http.get('/content/api/notfound')
      .then(function(response){
        $scope.content = response.data
      })
      .catch(function(error){
        console.log(error);
      });
        //.success(function(response) { $scope.content = response })
        //.error(function() { console.log('Error when getting content'); });
  }
