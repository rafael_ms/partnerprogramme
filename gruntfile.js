module.exports = function (grunt) {
    grunt.initConfig({

    // define source files and their destinations
    uglify: {
        files:{
            src: 'precomp/annotated/bundle.annotated.js'  // source files mask
            ,dest: 'client/public/dist/bundle.js'    // destination folder
            ,expand: false    // allow dynamic building
            ,flatten: true   // remove all unnecessary nesting
            //ext: '.min.js'   // replace .js to .min.js
        }
    }
    ,concat: {
        js: {
          src: [ 'bower_components/jquery/dist/jquery.js'
                ,'bower_components/angular/angular.js'
                ,'bower_components/angular-route/angular-route.js'
                ,'bower_components/angular-sanitize/angular-sanitize.min.js'
                ,'bower_components/bootstrap/dist/js/bootstrap.min.js'
                ,'precomp/annotated/precomp/angular/app.module.annotated.js'
                ,'precomp/annotated/precomp/angular/app.config.annotated.js'
                ,'precomp/annotated/precomp/angular/components/**/*.js'
                ]
          ,dest: 'precomp/annotated/bundle.annotated.js'
        }
    }
    ,ngAnnotate: {
        options: {
            singleQuotes: true
        },
        app: {
            files: [{
                expand: true
                ,src: 'precomp/angular/**/*.js'
                ,dest: './precomp/annotated'
                ,ext: '.annotated.js'
                ,extDot: 'last'
            }],
        }
    }
    ,watch: {
        js:  { files: 'precomp/angular/**/*.js', tasks: [ 'uglify' ] }
    }
});

// load plugins
grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-contrib-concat');
grunt.loadNpmTasks('grunt-ng-annotate');

// register at least this one task
grunt.registerTask('default', [ 'ngAnnotate','concat','uglify']);


};