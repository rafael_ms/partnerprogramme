'use strict';

// Register `tripDetail` component, along with its associated controller and template

angular.
  module('trips',['ngRoute'])
  .controller('tripsController', tripsController )
  .component('trips', {
    templateUrl: '/partials/content/content_trips'
    ,controller: 'tripsController'
    });

    function tripsController($scope,$http,$routeParams,$window){

        $scope.clickAccordion = function($event){
            var btn = $($event.currentTarget);
            var divCollap = $(btn.closest('div').next());

            if(btn.hasClass('open'))
                btn.removeClass('open');
            else
                btn.addClass('open');

            divCollap.collapse('toggle');
            $event.preventDefault();
        };

        $http.get('/content/api/get-trip/' + $routeParams.tripId)
        .then(function(response){
          if(response.data.length == 0)
            $window.location.href = '/notfound';

            $scope.content = response.data;
        })
        .catch(function(error){
          console.log(error);
        });
          //.success(function(response) { $scope.content = response })
          //.error(function() { console.log('Error when getting content'); })
          /*.catch(function(error){
              console.log(error);
          });*/
      }