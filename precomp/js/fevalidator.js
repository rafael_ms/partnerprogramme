    $("input, select").focus(function(a) {
        $(a.target).removeClass("error");
        $(".message,.message p").addClass("hide alert-danger").removeClass('alert-success');
        $(".message p.custom").remove();
    });
    $('input[type="phone"]').keypress(function(a) {
        var b = new RegExp(/[^0-9]/);
        b.test($(this).val()) && 46 != a.keyCode && 8 != a.keyCode && a.preventDefault()
    });
    $("form").submit(function(a) {
        if (0 == $("input[required='required']", this).filter(function() {
            return 0 == $.trim($(this).val()).length
        }).length)
            if (validEmail = !0,
            $("input[type='email']", this).filter(function() {
                validateEmail($(this).val()) || ($("span.icon-validation", $(this).parent()).removeClass("invisible"),
                validEmail = !1)
            }),
            (validEmail = !1),
            validEmail) {
                $("button", this).attr("disabled", "true");
                var form = new FormData(a.currentTarget);
                form.append('required',$("input[required='required'], select[required='required']").map(function(){ return this.id }).get());

                $.ajax({
                   type:"POST",
                   url:"register",
                   data: form,
                   processData: !1,
                   cache: !1,
                   contentType: !1,
                   success:function(msg){
                        var response = JSON.parse(msg);
                        if(response[0] == false){
                            $(".message").removeClass("hide");
                            $(".message").append("<p class='custom'>" + response[1] + "</p>");
                            errorHighlight(response[2]);
                            scrollTop($('.jumbotron').height());
                        }
                        else if(response[0] == true){
                            success(response[1]);
                        }
                        $("button", this).attr("disabled", "false");
                    },
                    error:function(a,b){
                        //console.log(a);
                    }
                });
                a.preventDefault();
            } else
                validEmail = !0,
                $(".message,.message p.email").removeClass("hide");
        else{
            highlightEmpty("input[required='required'], select[required='required']");
            $(".message,.message p.empty").removeClass("hide invisible");
            scrollTop($('.jumbotron').height());
        }

        a.preventDefault()
    });
    validateEmail = function(a) {
        var b = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return b.test(a)
    };
    function errorHighlight(error){
        switch(error) {
            case '#10001':
                highlightGeneric("input[required='required'], select[required='required']");
                break;
            case '#10002':
                highlightEmpty("input[required='required'], select[required='required']");
                break;
            case '#10003':
                highlightGeneric("input[type='email']");
                break;
            case '#10004':
                highlightGeneric("input[type='phone']");
                break;
        }
    }

    success = function(message){
        scrollTop(0);
        $('form')[0].reset();
        var $element = $($('form .collapse')[1]);
        $('button',$element.parent()).removeClass('open');
        $element.remove();
        $(".message").removeClass("hide alert-danger").addClass('alert-success');
        $(".message").append("<p class='custom'>" + message + "</p>");
    }

    highlightGeneric = function(classes){
        $(classes, 'form').filter(function() {
                $(this).addClass('error');
        });
    }

    highlightEmpty = function(classes){
        $(classes, 'form').filter(function() {
                if($(this).is('select') && (typeof $(this).val() === undefined || $(this).val() == null) )
                    $(this).addClass('error');
                else if( $(this).val().length == 0 )
                    $(this).addClass('error');
        });
    }

    scrollTop = function(y){
        //window.scrollTo(x, y);
        $("html,body").animate({
            scrollTop: y
        }, 500)
    }